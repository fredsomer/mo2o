<?php

namespace App\Controller;

use Curl\Curl;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    private $urlApi;
    private $curl;

    public function __construct()
    {
        $this->urlApi = 'https://api.punkapi.com/v2/';
        $this->curl = new Curl();
    }

    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        return $this->render('api/index.html.twig', [
            'controller_name' => 'ApiController',
        ]);
    }

    /**
     * @Route("/api/data_by_filter", name="data_by_filter", methods={"POST"})
     */
    public function getDataByFilter(Request $request): Response
    {
        $data = $request->request->get('food');

        $petition = $this->curl->get($this->urlApi.'beers?food='.$data);
        $this->curl->close();

        $dataArray = json_decode($petition->response);

        $response = array();
        foreach ($dataArray as $key) {
            $response[] = array(
                'id' => $key->id,
                'name' => $key->name,
                'description' => $key->description,
            );
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/api/data_by_filter_details", name="data_by_filter_details", methods={"POST"})
     */
    public function getDataByFilterDetails(Request $request): Response
    {
        $data = $request->request->get('food');

        $petition = $this->curl->get($this->urlApi.'beers?food='.$data);
        $this->curl->close();

        $dataArray = json_decode($petition->response);

        $response = array();
        foreach ($dataArray as $key) {
            $response[] = array(
                'id' => $key->id,
                'name' => $key->name,
                'description' => $key->description,
                'image' => $key->image_url,
                'slogan' => $key->tagline,
                'created_at' => $key->first_brewed,
            );
        }

        return new JsonResponse($response);
    }

}
